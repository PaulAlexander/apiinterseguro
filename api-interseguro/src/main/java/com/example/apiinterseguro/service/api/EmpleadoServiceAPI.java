package com.example.apiinterseguro.service.api;

import com.example.apiinterseguro.commons.GenericServiceAPI;
import com.example.apiinterseguro.model.Empleado;

public interface EmpleadoServiceAPI extends GenericServiceAPI<Empleado, Integer> {

}
