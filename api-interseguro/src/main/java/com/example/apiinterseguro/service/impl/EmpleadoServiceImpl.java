package com.example.apiinterseguro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.example.apiinterseguro.commons.GenericServiceImpl;
import com.example.apiinterseguro.dao.api.EmpleadoRepository;
import com.example.apiinterseguro.model.Empleado;
import com.example.apiinterseguro.service.api.EmpleadoServiceAPI;

@Service
public class EmpleadoServiceImpl  extends GenericServiceImpl <Empleado, Integer> implements EmpleadoServiceAPI{
	
	@Autowired
	private EmpleadoRepository empleadoRepository;
	
	@Override
	public CrudRepository<Empleado, Integer> getDao() {
		// TODO Auto-generated method stub
		return empleadoRepository;
	}

}
