package com.example.apiinterseguro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiInterseguroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInterseguroApplication.class, args);
	}

}
