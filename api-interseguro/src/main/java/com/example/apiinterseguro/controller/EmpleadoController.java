package com.example.apiinterseguro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.apiinterseguro.model.Empleado;
import com.example.apiinterseguro.service.api.EmpleadoServiceAPI;


@RestController
@RequestMapping(value="/talentfestapi")
@CrossOrigin("*")
public class EmpleadoController {
	
	@Autowired
	private EmpleadoServiceAPI empleadoServiceAPI;

	@GetMapping(value = "/empleados")
	public List<Empleado> getAll() {
		return empleadoServiceAPI.getAll();
	}
	

	@PostMapping(value = "/empleados")
	public ResponseEntity<Empleado> save(@RequestBody /*@Valid*/ Empleado empleado) {
		Empleado obj = empleadoServiceAPI.save(empleado);
		return new ResponseEntity<Empleado>(obj, HttpStatus.OK);
	}

	/*@GetMapping(value = "/empleado/delete/{id}")
	public ResponseEntity<Empleado> delete(@PathVariable Integer id) {
		Empleado persona = empleadoServiceAPI.get(id);
		if (persona != null) {
			empleadoServiceAPI.delete(id);
		} else {
			return new ResponseEntity<Empleado>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Empleado>(persona, HttpStatus.OK);
	}*/
	
	
}
