package com.example.apiinterseguro.dao.api;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.apiinterseguro.model.Empleado;

public interface EmpleadoRepository extends MongoRepository<Empleado, Integer>{

}
